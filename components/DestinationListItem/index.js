import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import Button from 'react-native-button';

const DestinationListItem = ({ destination, position, onDelete }) => {

  // TODO: allow for this to be draggable and re-orderable
  return (
    <View style={styles.container}>
      <View style={styles.position}>
        <Text>{position}</Text>
      </View>
      <View style={{ flex: 1 }}>
        <Text>{destination.title}</Text>
      </View>
      <Button
        containerStyle={styles.button}
        onPress={() => {
          onDelete(destination.id);
        }}
      >
        <Text>Delete</Text>
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: 70,
    padding: 10,
    borderWidth: 1,
    marginBottom: 10,
  },
  position: {
    width: 20,
    height: 20,
    marginRight: 5,
    borderRadius: 50,
    borderWidth: 1,
    fontWeight: 'bold',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    display: 'flex',
    width: 60,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
    paddingLeft: 5,
    paddingRight: 5,
    borderWidth: 1,
    backgroundColor: '#F00',
  }
});

export default DestinationListItem;
