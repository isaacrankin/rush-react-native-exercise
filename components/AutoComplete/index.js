import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {
  Text,
  StyleSheet,
} from 'react-native';
import Button from 'react-native-button';

const AutoComplete = ({ GooglePlacesAPIKey, onSelect, onSave, enableSave }) => {
  // keep a reference to GooglePlacesAutocomplete
  let autocompleteInstance;

  // TODO: exlcude selected locations from the results - aviod collisions

  return (
    <GooglePlacesAutocomplete
      ref={(instance) => {
        if (!autocompleteInstance) autocompleteInstance = instance;
      }}
      placeholder='Search for an address...'
      minLength={2}
      autoFocus={false}
      returnKeyType={'default'}
      fetchDetails={true}
      listViewDisplayed={false}
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        // save selection to state
        onSelect({
          id: data.place_id,
          title: data.description,
          latlng: {
            latitude: details.geometry.location.lat,
            longitude: details.geometry.location.lng,
          }
        });
      }}
      currentLocation={true}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: GooglePlacesAPIKey,
        language: 'en', // language of the results
        types: 'address', // default: 'geocode'
        components: 'country:nz',
      }}
      styles={styles}
      renderRightButton={() =>
        enableSave &&
        (
          <Button
            containerStyle={styles.saveButton}
            onPress={() => {
              if (typeof onSave === 'function') {
                autocompleteInstance.setAddressText('');
                onSave();
              }
            }}
          >
            <Text styles={{color: '#FFF'}}>Save</Text>
          </Button>)
      }
      />
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 0,
    height: 40,
  },
  saveButton: {
    padding: 10,
    height: 40,
    marginTop: 8,
    borderWidth: 1,
    marginLeft: 5,
    overflow: 'hidden',
    borderWidth: 1,
    backgroundColor: '#0F0',
  },
  listView: {
    position: 'absolute',
    width: '100%',
    top: 44,
    left: 0,
    backgroundColor: '#FFF',
    borderWidth: 1,
  },
  textInputContainer: {
    height: 40,
    margin: 0,
    backgroundColor: '#FFF',
    borderTopWidth: 0,
    borderBottomWidth:0
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 0,
    marginLeft: 0,
    marginRight: 0,
    height: 40,
    color: '#5d5d5d',
    fontSize: 16,
  },
  predefinedPlacesDescription: {
    color: '#1faadb'
  },
});

export default AutoComplete;
