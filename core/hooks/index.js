import { useContext } from 'react';
import { CoreContext } from '../context';
import {
  updateDestinations,
  deleteDestination,
} from '../actions';

export const useCore = () => {
  const context = useContext(CoreContext);

  if (!context) {
    throw new Error(`useCore must be used within a CoreProvider`);
  }

  const [state, dispatch] = context;

  const onUpdateDestinations = (data) =>
    dispatch(updateDestinations(data));

  const onDeleteDestination = (id) =>
    dispatch(deleteDestination(id));

  return {
    state,
    dispatch,
    onUpdateDestinations,
    onDeleteDestination,
  };
};
