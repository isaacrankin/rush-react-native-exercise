import * as WebBrowser from 'expo-web-browser';
import React, { useState, useContext, useEffect } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import MapView from 'react-native-maps'
import Button from 'react-native-button';
import Map from 'src/components/Map/index';
import AutoComplete from 'src/components/AutoComplete/index';
import DestinationListItem from 'src/components/DestinationListItem/index';
import { useCore } from '../core/hooks/';

export default DestinationsScreen = () => {
  const { state, onUpdateDestinations, onDeleteDestination } = useCore();
  const [ selection, setSelection ] = useState(null);
  const [ showList, setShowList ] = useState(true);

  // add selection to map destinations but with a pending flag
  const mapDestinations = selection ? state.destinations.concat({
    ...selection,
    pending: true,
  }) : state.destinations;

  const mapRegion = selection ? selection.latlng : null;

  const onSave = () => {
    if (selection) {
      // Save selection to global state
      onUpdateDestinations([selection]);
      // reset selection
      setSelection(null);
    }
  };

  return (
    <>
      <View style={styles.search} overflow="visible">
        <Text>Search for a location</Text>
        <AutoComplete
          GooglePlacesAPIKey={state.config.GOOGLE_PLACES_API_KEY}
          onSelect={setSelection}
          enableSave={!!selection}
          onSave={onSave}
        />
      </View>
      <Map
        destinations={mapDestinations}
        region={mapRegion}
        GoogleMapsAPIKey={state.config.GOOGLE_MAPS_API_KEY}
      />
      {
        state.destinations.length > 0 && (
          <Button
            containerStyle={styles.destinationsListButton}
            onPress={() => {
              setShowList(!showList);
            }}
          >
            <Text styles={{color: '#FFF'}}>{showList ? 'Hide' : 'Show'} List</Text>
          </Button>
        )
      }
      {
        showList && state.destinations.length > 0 && (
          <>
            <View style={styles.destinationsList}>
              <Text style={{ fontSize: 22, textAlign: 'center', marginBottom: 10 }}>Destinations</Text>
              <ScrollView>
                {state.destinations.map((destination, key) => (
                  <DestinationListItem
                    key={destination.id}
                    position={key + 1}
                    destination={destination}
                    onDelete={onDeleteDestination}
                  />
                ))}
              </ScrollView>
            </View>
          </>
        )
      }
    </>
  );
}

DestinationsScreen.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  search: {
    position: 'absolute',
    top: 60,
    left: '5%',
    zIndex: 100,
    width: '90%',
    borderWidth: 1,
    height: 100,
    padding: 15,
    backgroundColor: '#FFF',
    overflow: 'visible',
  },
  destinationsList: {
    position: 'relative',
    padding: 20,
    paddingLeft: 10,
    paddingRight: 10,
    maxHeight: 300,
    backgroundColor: '#FFF',
  },
  destinationListTitle: {
    fontSize: 22,
    textAlign: 'center',
    marginBottom: 10,
  },
  destinationsListButton: {
    position: 'absolute',
    bottom: 300,
    right: 5,
    padding: 8,
    borderWidth: 1,
    backgroundColor: '#FFF',
  }
});
