import React from 'react';
import { Platform } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import DestinationsScreen from '../screens/DestinationsScreen';
import DirectionsScreen from '../screens/DirectionsScreen';

const DestinationsStack = createStackNavigator({
  Destinations: DestinationsScreen,
});

DestinationsStack.navigationOptions = {
  tabBarLabel: 'Destinations',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const DirectionsStack = createStackNavigator({
  Directions: DirectionsScreen,
});

DirectionsStack.navigationOptions = {
  tabBarLabel: 'Directions',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

export default createBottomTabNavigator({
  DestinationsStack,
  // DirectionsStack,
});
