export const UPDATE_DESTINATIONS = 'update_destinations';

export const DELETE_DESTINATIONS = 'delete_destination';

export const UPDATE_USER_LOCATION = 'update_location';
