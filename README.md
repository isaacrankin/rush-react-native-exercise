# Foot Traffic

This app lets you add custom locations to a map using an auto compelete search and see directions to visit those locations.

## Setup

1. Create an `.env.json` file based on the example `.example-env.json` file.
2. Run `yarn`
3. Run `yarn start`
