import React, { useMemo, useReducer } from 'react';
import reducer, { defaultState } from '../reducer';

export const CoreContext = React.createContext();

export const CoreProvider = props => {
  // TODO: find a way to persist state?
  const [state, dispatch] = useReducer(reducer, defaultState);
  const value = useMemo(() => [state, dispatch], [state]);
  return (
    <CoreContext.Provider
      value={value}
      {...props} />
  );
};
