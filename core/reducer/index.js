import {
  UPDATE_DESTINATIONS,
  UPDATE_USER_LOCATION,
  DELETE_DESTINATION,
} from '../constants';

import config from 'src/.env.json';

export const defaultState = {
  destinations: [],
  userLocation: null,
  config,
};

// TODO: find a way to persist state?

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_DESTINATIONS:
      return {
        ...state,
        destinations: [
          ...state.destinations,
          ...action.payload,
        ],
      };
    case UPDATE_USER_LOCATION:
      return {
        ...state,
        userLocation: action.payload,
      };
    case DELETE_DESTINATION:
      return {
        ...state,
        destinations: [
          ...state.destinations.filter(destination => destination.id !== action.payload),
        ],
      };
    default:
      return state;
  }
};

export default reducer;
