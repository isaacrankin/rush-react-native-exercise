import React from 'react';
import {
  StyleSheet,
  Text,
} from 'react-native';
import Button from 'react-native-button';
import MapView from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions';

const predefinedLocations = {
  aucklandCentral: {
    latitude: -36.8485,
    longitude: 174.7633,
    // TODO: figure out what these do?
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  },
};

const Map = ({ GoogleMapsAPIKey, destinations, region = null }) => {

  const markerColourDefault = '#0F0';
  const markerColourPending = '#FFA500';
  const props = {};

  // optional pre-defined region/map centre
  if (region) {
    props.region = {
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
      ...region,
    };
  }

  let origin;
  let destination;
  let waypoints = [];

  // if more than 1 destinations use these to plot directions
  if (destinations.length >= 2) {
    origin = {
      ...destinations[0].latlng,
    };
    destination = {
      ...destinations[destinations.length - 1].latlng,
    };
    waypoints = destinations.slice(1, -1).map(dest => dest.latlng);
  }

  // Use this to fit map...
  // ref={mapRef => mapRef === null ? null : mapRef.fitToElements(!!region) }

  return (
    <MapView
      style={styles.map}
      initialRegion={ predefinedLocations.aucklandCentral }
      {...props}
    >
      {destinations.map(marker => (
        <MapView.Marker
          ref={(marker) => {
            // this delay allows for the marker to exist before opening the callout
            // find a better way of doing this :/
            setTimeout(() => {
              if (marker) marker.showCallout();
            }, 300);
          }}
          key={marker.id}
          pinColor={marker.pending ? markerColourPending : markerColourDefault}
          coordinate={marker.latlng}
          title={marker.title}
          description={marker.description}
        >
          <MapView.Callout>
            <Text>{marker.title}</Text>
          </MapView.Callout>
        </MapView.Marker>
      ))}
      {
        origin && destination && (
          <MapViewDirections
            origin={origin}
            destination={destination}
            apikey={GoogleMapsAPIKey}
            waypoints={waypoints}
          />
        )
      }

      </MapView>
  );
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});

export default Map;
