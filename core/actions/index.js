import {
  UPDATE_DESTINATIONS,
  UPDATE_USER_LOCATION,
  DELETE_DESTINATION,
} from '../constants';

export const updateDestinations = (payload) => ({
  type: UPDATE_DESTINATIONS,
  payload,
});

export const deleteDestination = (payload) => ({
  type: DELETE_DESTINATION,
  payload,
});

export const updateUserLocation = (payload) => ({
  type: UPDATE_USER_LOCATION,
  payload,
});
