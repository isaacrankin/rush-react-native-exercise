import * as WebBrowser from 'expo-web-browser';
import React, { useContext } from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import { MonoText } from '../components/StyledText';
import { CoreContext } from '../core/context/';

export default function DirectionsScreen() {
  const [ state, dispatch ] = useContext(CoreContext);
  console.log(state);
  return (
    <View>
      <Text>Directions</Text>
    </View>
  );
}

DirectionsScreen.navigationOptions = {
  header: null,
};
